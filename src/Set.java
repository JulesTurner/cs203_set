import java.io.*;
import java.util.*;
import java.util.ArrayList;
import java.util.LinkedList;
		public class Set<E> {
			private LinkedList<E> list; 
			
		Set() {
		// the constructor creates an empty set
			list = new LinkedList<E>();
		}
		
		public boolean isEmpty() {
		// determines whether this set is empty
			if( list.size() == 0){
				return true;
			}
			else return false;
		}
		
		public int size() {
		// returns the number of elements in this set (its cardinality)
			return list.size();
		}
		
		public void add(E item) {
		// adds the specified integer item to this set if it is NOT already present
		// the items in the set do NOT need to be in a sorted order
				if(contains(item) == true){	
				}
				else list.add(item);			
		}
		
		public boolean contains(E item) {
		// determines if this set contains the specified item
			for (int i = 0; i< list.size(); i++) {
				E num = list.get(i);
				if (num.equals(item)) {
					return true;
				}
			}
			return false;
		}
		
		E get(int index) {
			return list.get(index);
		}
		
		Set<E> union(Set<E> inputSet) {
		// creates a new set containing all of the elements of this current set
		// and the other inputSet (no duplicates),
		// returns the resulting set
			Set<E> j = new Set<E>(); 
			for (int x = 0; x<list.size(); x++){
				j.add(list.get(x));	
			}
			for (int l = 0; l<inputSet.size(); l++){
				if (!j.contains(inputSet.get(l))){
					j.add (inputSet.get(l));
				}
			}
			return j;
		}
		
		Set<E> intersection(Set<E> inputSet) {
		// creates a new set of elements that appear in both this set
		// and the other set,
		// returns the resulting set
			Set<E> t = new Set<E>();
			for (int w = 0; w< list.size(); w++){
				if (inputSet.contains (list.get(w)) == true){
					t.add (list.get(w));
				}
			}
			return t;
		}
		
		void removeAll() {
		// removes all of the items in this set
			list.clear();
		}
		
		void display() {
		// displays all items in the set
			for (int i= 0; i<list.size(); i++){
				System.out.println(list.get(i));
			}
		}
	
	public static void main (String [] args) {
		Set<Integer> s = new Set<Integer>(); // creates a new empty set
		System.out.println(s.isEmpty()); // makes sure that the set is empty right now
		s.add(5); // adds 5
		s.add(-4); // adds -4
		s.add(3); // adds 3
		s.add(5); // should NOT add 5 to the set because it already contains it
		System.out.println(s.contains(-4)); // should be true
		System.out.println(s.size()); // should print 3
		
		Set<Integer> g = new Set<Integer>(); // creates a new empty set
		g.add(7); // adds 7
		g.add(-3); // adds -3
		g.add(3); // adds 3
		
		Set<Integer> j = s.union(g);
		j.display(); // should display 5, -4, 3, 7, -3
		Set<Integer> r = s.intersection(g);
		r.display(); // should display 3
	}
}